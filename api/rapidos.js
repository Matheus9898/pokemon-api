/*
    Lista apenas os pokemons com mais de 140 pontos de velocidade (speed)
    A lista deve estar ordenada do mais rápido para o mais lento
*/

const data = require('../data')

module.exports = function(req, res){

    let result = "pokemon list"

    result = data.filter(function(pokemon){
        return pokemon.speed > 140
    })
    
    //Retorno
    res.json(result)
}
